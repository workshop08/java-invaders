package Physics;

import java.lang.reflect.Field;
import GameField.*;
import Entity.*;

public class GamePhysics {
    GameField fieldModel;

    public GamePhysics(GameField fieldModel){
        this.fieldModel = fieldModel;
    }


    public void moveRight(Entity entity){
        if (entity.getPosX() < fieldModel.getWidth() - 1){
            if (fieldModel.getEntity(entity.getPosX() + 1, entity.getPosY()) != null){
                collision(entity, fieldModel.getEntity(entity.getPosX() + 1, entity.getPosY()));
            }
            fieldModel.setEntity(entity.getPosX(), entity.getPosY(), null);
            fieldModel.setEntity(entity.getPosX() + 1, entity.getPosY(), entity);
            entity.setPosX(entity.getPosX() + 1);
        }
    }

    public void moveLeft(Entity entity){
        if (entity.getPosX() > 0){
            if (fieldModel.getEntity(entity.getPosX() - 1, entity.getPosY()) != null){
                collision(entity, fieldModel.getEntity(entity.getPosX() - 1, entity.getPosY()));
            }
            fieldModel.setEntity(entity.getPosX(), entity.getPosY(), null);
            fieldModel.setEntity(entity.getPosX() - 1, entity.getPosY(), entity);
            entity.setPosX(entity.getPosX() - 1);
        }
    }

    public void moveUp(Entity entity){
        if (entity.getPosY() > 0){
            if (fieldModel.getEntity(entity.getPosX(), entity.getPosY() - 1) != null){
                collision(entity, fieldModel.getEntity(entity.getPosX(), entity.getPosY() - 1));
            }
            fieldModel.setEntity(entity.getPosX(), entity.getPosY(), null);
            fieldModel.setEntity(entity.getPosX(), entity.getPosY() - 1, entity);
            entity.setPosY(entity.getPosY() - 1);
        }
    }

    public void moveDown(Entity entity){
        if (entity.getPosY() < fieldModel.getHeight() - 1){
            if (fieldModel.getEntity(entity.getPosX(), entity.getPosY() + 1) != null){
                collision(entity, fieldModel.getEntity(entity.getPosX(), entity.getPosY() + 1));
            }
            fieldModel.setEntity(entity.getPosX(), entity.getPosY(), null);
            fieldModel.setEntity(entity.getPosX(), entity.getPosY() + 1, entity);
            entity.setPosY(entity.getPosY() + 1);
        }
    }

    public void collision(Entity entity, Entity entity1){
        int min = Math.min(entity.getHp(), entity1.getHp());
        entity.getDamage(min);
        entity1.getDamage(min);
    }

}
