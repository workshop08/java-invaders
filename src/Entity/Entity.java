package Entity;

import GameField.GameField;
import Physics.GamePhysics;

import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;

public class Entity extends Thread
{
    protected int speed, x, y, hp;
    protected boolean isAlive;
    protected GameField gameField;

    private SwingPropertyChangeSupport pcSupport = new SwingPropertyChangeSupport(this);

    public void setPosX(int x)
    {
        this.x = x;
    }

    public void setPosY(int y)
    {
        this.y = y;
    }

    public int getPosX()
    {
        return x;
    }

    public int getPosY()
    {
        return y;
    }

    public void isAlive(boolean a) {
        this.isAlive = a;
    }

    public void setSpeed(int speed)
    {
        this.speed = speed;
    }

    public int getSpeed()
    {
        return speed;
    }

    public void setHp(int hp)
    {
        this.hp = hp;
    }

    public int getHp()
    {
        return hp;
    }

    public void getDamage(int damage)
    {
        int oldValue = hp;
        this.hp -= damage;
        int newValue = hp;
        pcSupport.firePropertyChange("HP", oldValue, newValue);
    }

    public void moveUp()
    {
        int oldValue = y;
        gameField.getPhysics().moveUp(this);
        int newValue = y;
        pcSupport.firePropertyChange("Y", oldValue, newValue);
    }

    public void moveDown()
    {
        int oldValue = y;
        gameField.getPhysics().moveDown(this);
        int newValue = y;
        pcSupport.firePropertyChange("Y", oldValue, newValue);
    }

    public void moveRight()
    {
        int oldValue = x;
        gameField.getPhysics().moveRight(this);
        int newValue = x;
        pcSupport.firePropertyChange("X", oldValue, newValue);
    }

    public void moveLeft()
    {
        int oldValue = x;
        gameField.getPhysics().moveLeft(this);
        int newValue = x;
        pcSupport.firePropertyChange("X", oldValue, newValue);
    }

    public void shoot(int x, int y)
    {
        gameField.addShot(new Shot(gameField, 10,10, x, y));
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcSupport.addPropertyChangeListener(name, listener);
    }

}