package Entity;

import GameField.GameField;

public class Enemy extends Entity
{

    protected int shootCoolDown;

    public Enemy(GameField gameField, int speed, int hp, int x, int y)
    {
        this.gameField = gameField;
        this.hp = hp;
        this.speed = speed;
        this.x = x;
        this.y = y;
    }

    public void run() {
        int c = 0;
        synchronized (this.gameField.getPhysics()) {
            c++;
            if (c == shootCoolDown) {
                c = 0;
                this.moveDown();
            }
            this.shoot(x, y + 2);
            try {
                this.wait(this.speed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
