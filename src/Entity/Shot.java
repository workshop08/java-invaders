package Entity;

import GameField.GameField;

public class Shot extends Entity
{
    protected int damage;

    public void setDamage(int damage)
    {
        this.damage = damage;
    }

    public int getDamage()
    {
        return damage;
    }

    public Shot(GameField gameField, int speed, int hp, int x, int y)
    {
        this.gameField = gameField;
        this.hp = hp;
        this.speed = speed;
        this.x = x;
        this.y = y;
    }

    public void run() {
        synchronized (this.gameField.getPhysics()) {
            this.moveDown();
            try {
                this.wait(this.speed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
