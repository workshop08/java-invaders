package Entity;

import GameField.GameField;

public class Player extends Entity
{
    public Player(GameField gameField, int speed, int hp, int x, int y)
    {
        this.gameField = gameField;
        this.hp = hp;
        this.x = x;
        this.y = y;
    }
}
