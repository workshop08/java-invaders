package GameField;

import Entity.Entity;
import Entity.Shot;
import Entity.Enemy;
import Entity.Player;
import Physics.GamePhysics;

import java.util.ArrayList;

public class GameField {
    public int width;
    public int height;
    Entity[][] field;
    ArrayList<Enemy> enemyList;
    GamePhysics physics;
    ArrayList<Shot> shotList;
    Player player;
    Generator generator;
    public ArrayList<Enemy> getEnemyList() {
        return enemyList;
    }

    public ArrayList<Shot> getShotList() {
        return shotList;
    }

    public GameField(int w,int h){
        width = w;
        height = h;
        field = new Entity[height][width];
        enemyList = new ArrayList();
        shotList = new ArrayList();
        physics = new GamePhysics(this);
        player = new Player(this, 10, 3, 0, width / 2);
        generator = new Generator(this);
    }

    public void addShot(Shot shot){
        shotList.add(shot);
        shot.start();
    }

    public void start(){
        generator.start();
    }

    public GamePhysics getPhysics() {return physics;}
    public int getHeight(){
        return height;
    }
    public void setEntity(int x,int y,Entity en){
        field[x][y] = en;
    }
    public int getWidth(){
        return width;
    }
    public Entity getEntity(int x,int y){
        return field[x][y];
    }
    public Player getPlayer () {return player;}
}
