package Graphics;

import Entity.Enemy;
import Entity.Player;
import Entity.Shot;
import GameField.GameField;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import Control.Control;
public class GameGraphics extends Application {

    private static double windowHeight;
    private static double windowWidth;
    private GameField field;
    private Player player;
    private static double f_x;
    private static double f_y;

    public GameGraphics(GameField field, double x, double y) {
        this.field = field;
        this.player = field.getPlayer();
        this.windowWidth = x;
        this.windowHeight = y;
        this.f_x = windowWidth / field.getWidth();
        this.f_y = windowHeight / field.getHeight();
    }

    public void move(ImageView img, double x, double y) {
        Timeline timeline = new Timeline();
        KeyValue xValue = new KeyValue(img.xProperty(), x);
        KeyValue yValue = new KeyValue(img.yProperty(), y);
        KeyFrame keyFrame = new KeyFrame(Duration.millis(100), xValue, yValue);
        timeline.getKeyFrames().add(keyFrame);
        timeline.play();
    }

    public void start(Stage primaryStage) throws FileNotFoundException {

        Group root = new Group();
        Scene scene = new Scene(root, windowWidth, windowHeight);
        Control control = new Control(scene, player);
        primaryStage.setTitle("Java Invaders");
        primaryStage.setScene(scene);
        Map<Enemy, ImageView> enemyViewDict = new HashMap<Enemy, ImageView>();
        Map<Shot, ImageView> shotViewDict = new HashMap<Shot, ImageView>();
        ObservableList rootChildren = root.getChildren();

        Image playerImg = new Image("https://cdn.discordapp.com/attachments/731787258977320990/736247383482040520/player.png");
        Image enemyImg = new Image("https://cdn.discordapp.com/attachments/731787258977320990/736247389006069881/enemy.png");
        Image shotImg = new Image("https://cdn.discordapp.com/attachments/731787258977320990/736247384308318308/shot.png");
        Image backgroundImg = new Image("https://cdn.discordapp.com/attachments/731787258977320990/736247387290599556/background.gif");

        ImageView backgroundView = new ImageView(backgroundImg);
        backgroundView.setFitHeight(windowHeight);
        backgroundView.setFitWidth(windowWidth);
        rootChildren.add(backgroundView);




        ImageView playerView = new ImageView(playerImg);
        playerView.setFitHeight(50);
        playerView.setFitWidth(75);
        playerView.setY(windowHeight - 50);
        playerView.setX(0);

        Text hpText = new Text(10, 10, "HP - " + player.getHp());
        hpText.setFont(Font.font("century gothic", 20));
        hpText.setFill(Color.WHITE);
        player.addPropertyChangeListener("HP", pcEvent -> updateHp(hpText, player.getHp()));

//        Text scoreText = new Text(10, 10, "SCORE - " + player.score);
//        scoreText.setFont(Font.font("century gothic", 20));
//        scoreText.setFill(Color.WHITE);
//        player.addPropertyChangeListener(player.SCORE, pcEvent -> updateScore(hpText, player.score));

        player.addPropertyChangeListener("X", pcEvent -> move(playerView, (field.getPlayer().getPosX()) * f_x  + 0.2,
                field.getPlayer().getPosY() * f_y));
        player.addPropertyChangeListener("Y", pcEvent -> move(playerView, field.getPlayer().getPosX() * f_x,
                (field.getPlayer().getPosY()) * f_y  + 0.2));
        rootChildren.add(hpText);
        rootChildren.add(playerView);
        primaryStage.show();


        while (player.isAlive() == true) {

            for (int i = 0; i < field.getEnemyList().size(); i++) {
                if (enemyViewDict.containsKey(field.getEnemyList().get(i)) == false) {
                    ImageView enemyView = new ImageView(enemyImg);
                    enemyView.setFitHeight(f_y * 0.6);
                    enemyView.setFitWidth(f_x * 0.6);
                    enemyView.setX((field.getEnemyList().get(i).getPosX() + 0.2) * f_x);
                    enemyView.setY((field.getEnemyList().get(i).getPosY() + 0.2) * f_y);
                    enemyViewDict.put(field.getEnemyList().get(i), enemyView);
                    rootChildren.add(enemyViewDict.get(field.getEnemyList().get(i)));
                } else {
                    int finalI = i;
                    field.getEnemyList().get(i).addPropertyChangeListener("X", pcEvent -> move(enemyViewDict.get(field.getEnemyList().get(finalI)), (field.getEnemyList().get(finalI).getPosX() + 0.2) * f_x,
                            enemyViewDict.get(field.getEnemyList().get(finalI)).getY()));
                    field.getEnemyList().get(i).addPropertyChangeListener("Y", pcEvent -> move(enemyViewDict.get(field.getEnemyList().get(finalI)), enemyViewDict.get(field.getEnemyList().get(finalI)).getX(),
                            (field.getEnemyList().get(finalI).getPosX() + 0.2) * f_x));
                }
            }

            for (Enemy enemy : enemyViewDict.keySet()) {
                if (enemy.isAlive() == false) {
                    rootChildren.remove(enemyViewDict.get(enemy));
                    enemyViewDict.remove(enemy, enemyViewDict.get(enemy));
                }
            }

            for (int i = 0; i < field.getShotList().size(); i++) {
                if (shotViewDict.containsKey(field.getShotList().get(i)) == false) {
                    ImageView shotView = new ImageView(shotImg);
                    shotView.setFitHeight(f_y * 0.8);
                    shotView.setFitWidth(f_x * 0.2);
                    shotView.setX((field.getShotList().get(i).getPosX() + 0.4) * f_x);
                    shotView.setY((field.getShotList().get(i).getPosY() + 0.1) * f_y);
                    shotViewDict.put(field.getShotList().get(i), shotView);
                    rootChildren.add(shotViewDict.get(field.getShotList().get(i)));
                } else {
                    int finalI = i;
                    field.getShotList().get(i).addPropertyChangeListener("X", pcEvent -> move(shotViewDict.get(field.getShotList().get(finalI)), (field.getShotList().get(finalI).getPosX() + 0.2) * f_x,
                            shotViewDict.get(field.getShotList().get(finalI)).getY()));
                    field.getShotList().get(i).addPropertyChangeListener("Y", pcEvent -> move(shotViewDict.get(field.getShotList().get(finalI)), shotViewDict.get(field.getShotList().get(finalI)).getX(),
                            (field.getShotList().get(finalI).getPosX() + 0.2) * f_x));
                }
            }

            for (Shot shot : shotViewDict.keySet()) {
                if (shot.isAlive() == false) {
                    rootChildren.remove(shotViewDict.get(shot));
                    shotViewDict.remove(shot, shotViewDict.get(shot));
                }
            }

        }


    }

    private void updateHp(Text hpText, int hp) {
        hpText.setText("HP - " + hp);
    }

    private void updateScore(Text scoreText, int score) {
        scoreText.setText("SCORE - " + score);
    }

}
