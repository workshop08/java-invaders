package Control;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import Entity.Player;
import javafx.scene.input.KeyEvent;

public class Control {

    protected Scene primaryScene;
    protected Player player;

    public Control(Scene primaryScene, Player player) {
        this.primaryScene = primaryScene;
        this.player = player;
        EventHandler eventHandler = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                switch (keyEvent.getCode()){
                    case W: player.moveUp(); break;
                    case A: player.moveLeft(); break;
                    case S: player.moveDown(); break;
                    case D: player.moveRight(); break;
                    case SPACE: player.shoot(player.getPosX(), player.getPosY() - 1); break;
                    default: break;
                }
            }
        };
        primaryScene.addEventHandler(KeyEvent.KEY_PRESSED, eventHandler);
    }

}
