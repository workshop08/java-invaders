import GameField.GameField;
import Graphics.GameGraphics;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.FileNotFoundException;

public class Main extends Application {

    public void start(Stage primaryStage) throws FileNotFoundException {
        GameField gameField = new GameField(10, 10);
        GameGraphics view = new GameGraphics(gameField, 600, 600);
        view.start(primaryStage);
        gameField.start();
    }

    public static void main(String[] args)  {

        launch();

    }
}
